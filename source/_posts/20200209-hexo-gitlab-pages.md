---
title: Hexo 建置在 GitLab Pages 上
date: 2020-02-09 17:35:52
tags:
- Hexo
- GitLab Pages
---

## 前言

寫一些文章記錄程式筆記，比較了很多寫 Note 軟體，看來看去，還是網頁比較可以調出漂亮的程式碼區塊壓

> **GitLab Pages**: GitLab Pages is a feature that allows you to publish static websites directly from a repository in GitLab.

> **Hexo**: A fast, simple & powerful blog framework, powered by Node.js.
## Node.js 安裝

在全新安裝的 Ubuntu 18.04 環境底下執行以下指令

```bash
$ sudo apt-get install curl
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
$ sudo  apt-get install nodejs
```

接下來執行以下指令，我們來來看一下安裝好的版本

```bash
$ node -v
v10.18.1
$ npm -v
6.13.4
```

> 注意：如果你是使用 Windows Subsystem for Linux 的童鞋，那你有很大的機率遇到以下問題，如果沒遇到，恭喜你，可以直接跳到下一節

```bash
$ npm -v
: not foundram Files/nodejs/npm: 3: /mnt/c/Program Files/nodejs/npm:
: not foundram Files/nodejs/npm: 5: /mnt/c/Program Files/nodejs/npm:
/mnt/c/Program Files/nodejs/npm: 6: /mnt/c/Program Files/nodejs/npm: Syntax error: word unexpected (expecting "in")
```

別緊張，首先我們先來看一下 npm 的位置在哪裡

```bash
$ which npm
/usr/bin/npm
```

我們可以看到 npm 是位於 /usr/bin/npm 這個位置，但是上面的錯誤提示寫說我在 Program Files 裡面找不到 npm 阿

所以我們要在 PATH 裡面增加 /usr/bin，注意這個新增必須要在 $PATH 之前才會生效

打開 `~/.profile`，然後把 PATH 的地方用以下的指令取代

```bash
PATH="$HOME/bin:$HOME/.local/bin:/usr/bin:$PATH"
```

最後要記得 `source ~/.profile`

什麼都做好之後呢，重新開一個新的 console 來試試看囉

```bash
$ whereis npm
npm: /usr/bin/npm /mnt/c/Program Files/nodejs/npm /mnt/c/Program Files/nodejs/npm.cmd
$ node -v
v10.18.1
$ npm
6.13.4
```

現在 Node.js + npm 都己經安裝完成啦


## Hexo 安裝

一旦所有的必備軟體都安裝完畢後，即可透過 npm 安裝 Hexo

```bash
$ npm install -g hexo-cli
$ hexo init hexo
```

完成後可以看到 hexo 資料夾內有下列檔案（每個人看到的可能大同小異）

```bash
.
├ _config.yml
├ package-lock.json
├ package.json
├ scaffolds
│   ├ draft.md
│   ├ page.md
│   └ post.md
├ source
│   └ _posts
└ themes
    └ landscape
```

我們可以執行指令來開啟一個本地端的伺服器，預設是 [http://localhost:4000/](http://localhost:4000/)

```bash
$ hexo s
```

## Hexo 主題

預設的部落格雛形長得又老又醜，想當然我們會想要趕快換掉它，以免傷眼睛！

Hexo 有很多的主題包可以自行下載來使用

我這邊使用的是 [NexT](https://github.com/theme-next/hexo-theme-next) ，依照它的安裝建議來安裝即可

```bash
$ cd hexo
$ git clone https://github.com/theme-next/hexo-theme-next themes/next
```

> 注意：這邊 `themes/next` 請記得改成 `themes/landscape`，之後會提到為什麼

下載好之後呢，再次打開本地端伺服器看一下

```bash
$ hexo s
```

新的主題就會完美呈現！

## GitLab Pages 部署

到這裡只剩下最後一步了，那就是上傳整個部落格到 GitLab 上，讓別人可以看到網站！

我們要回到原本建立的資料夾

```bash
$ cd ~/hexo/
```

我們新建一個檔案 `.gitlab-ci.yml` 貼上以下內容，這個檔案的作用是告訴 GitLab CI 做自動化部屬的動作

```bash
image: node:10.16.0
cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - test -e package.json && npm install
  - hexo clean
  - hexo generate

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
```

再來

我們先下載 Git 套件並設定 config

```bash
$ sudo apt-get install git
$ git config --global user.name "你的GitLab用户名"
$ git config --global user.email "你的GitLab註冊郵箱"
$ ssh-keygen -t rsa -C "你的GitHub註冊郵箱"
```

好了之後我們到 `~/.ssh` 內找到 id_rsa.pub，把它複製貼上到 GitLab 設定裡面的SSH Keys 內

完成之後呢我們就可以對 GitLab 上 Git commit/push 啦～

我們到 GitLab 建立一個新的專案

請注意以下幾點

1. 新建一個專案 Project name 請務必填入 `<<您的帳號>>.gitlab.io`
2. Visibility Level 建議設成公開的
3. 專案之設定 Visibility, project features, permissions 請把 Pages 的權限設定成 Everyone 

建好新專案後，我們回到 `hexo/` 資料夾

```bash
$ git init
$ git remote add origin git@gitlab.com:<< your account >>/<< your account >>.gitlab.io.git
$ git add .
$ git commit -m "Initial commit"
$ git push -u origin master
```

大功告成

我們可去網站上看囉！

以下這個將會是你的網址！

<u>**https://<< your account >>.gitlab.io/**</u>



> 注意：前面提到把資料夾改成 `themes/landscape`
>
> 我在這邊曾經遇到一個問題，我使用 ＮexT 這個主題並且在上傳該主題檔案到 GitLab 之後發現該主題的資料夾沒有被上傳，導致我的 gitlab.io 完全沒有東西
>
> 網路上相關問題：
>
> [远程仓库无法备份theme/next主题的问题](https://github.com/iissnan/hexo-theme-next/issues/932)
>
> [問題 - Hexo 部署上 Github 後頁面全空了](https://kennyliblog.nctu.me/2019/06/19/Hexo-problem-Gitsubmodules/#Git-submodules)
>
> 後來找了很久，還是沒有解．．．
>
> 所以我把 next 的所有資料都丟到 landscape 內偽裝成 landscape 主題暫時這樣解決XD

## 參考資料

1. https://hexo.io/zh-tw/docs/
2. https://github.com/theme-next/hexo-theme-next
3. https://blog.typeart.cc/%E9%83%A8%E7%BD%B2HEXO%E5%88%B0GitLab%20Page/
4. https://hexo.io/zh-tw/docs/gitlab-pages

