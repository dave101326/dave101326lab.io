---
title: 條件分支（Elixir從零開始系列 04）（鼠年全馬鐵人挑戰 W05）
tags:
- w3HexSchool
- Elixir
- Elixir from 0 to 100
date: 2020-03-17 07:58:32
---

> 這是 w3HexSchool 鼠年全馬鐵人挑戰 Week 5 唷 

> 這是 Elixir 從零開始 系列 04 唷

## 前言

要寫這篇之前，回頭來看前面寫的幾篇文章，我發現其實我應該把 [Function & Module](https://dave101326.gitlab.io/2020/03/05/20200305-function-and-module/) 擺在這篇後面比較有從基礎到進階的感覺，不過算了懶得改，覺得奇怪的彭油記得自己跳著看壓 XD。

這一章節我們來介紹一些條件分支（Conditional branching）的語法，說白話點也就是程式語言最基本的 `if` 和 `else` 等

## if, else and unless

用法如下

```elixir
if condition do
	...
else
	...
end
```

當然我們也可以用一行解決

```elixir
if condition, do: something, else: another_thing
```

我們來看範例

```elixir
if value> 20 do
	IO.puts("Look !")
else
	IO.puts("Yo Yo")
end
```

再來看一個範例

```elixir
def max(a, b) do
	if a >= b, do: a, else: b
end
```

我們來介紹 `unless` 這個 macro，它其實就是 `if (not …)` 

利用上面範例

```elixir
def max(a, b) do
	unless a >= b, do: b, else: a
end
```

## cond

`cond` 也是一個macro，用法如下

```elixir
cond do
	expression_1 ->
	...
	
	expression_2 ->
	...
...
end
```

來看個範例

```elixir
def max(a, b) do
	cond do
		a >= b -> a
		true -> b
	end
end
```

這裡我們可以注意一下，上述程式加 `true -> b` 的緣由是為了讓剩下的條件都被 `true` 捕捉到，這種用法值得學習一下唷

## case

跟 `cond` 長得很像，但是其實是不一樣的，用法如下

```elixir
case expression do
	pattern_1 ->
		...
		
	pattern_2 ->
		...
...
end
```

來看一個範例

```elixir
def max(a,b) do
	case a >= b do
		true -> a
		_ -> b
	end
end
```

這裡跟上面的 `cond` 一樣，使用 `_` 可以捕捉剩下的其他條件，但如果沒有使用 `_` 比對時將失敗，進而引發一個錯誤訊息



想像一下 `_`  將會比對該行以上條件之外的所有條件，就很類似 C# 中的 `default` 或者我們可以說 *anything else*

```csharp
switch (caseSwitch)
      {
          case 1:
              Console.WriteLine("Case 1");
              break;
          case 2:
              Console.WriteLine("Case 2");
              break;
          default:
              Console.WriteLine("Default case");
              break;
      }
```

## 參考資料

1. https://elixir-lang.org/getting-started/case-cond-and-if.html#case
2. https://elixirschool.com/zh-hant/lessons/basics/control-structures/



