---
title: 啥？Elixir 是什麼？？？（Elixir從零開始系列 01）（鼠年全馬鐵人挑戰 W02）
tags:
- w3HexSchool
- Elixir
- Elixir from 0 to 100
date: 2020-02-24 21:32:39
---

> 這是 w3HexSchool 鼠年全馬鐵人挑戰 Week 2 唷 

> 這是 Elixir 從零開始 系列 01 唷

## 前言

Elixir 是什麼？它於 1983 年誕生於日本，帶著先進膠原蛋白保養技術問世。

https://www.elixirtw.com/zh-TW/about_story/index

誒誒誒？是不是走錯棚了吧？

相信大家第一個 Google 到的都是保養品網站

不過這不是我們要的

記得在輸入關鍵字的時候多打一個 `lang` 也就是 `elixir lang`

就像搜尋 Go 語言大家會打 `golang` 一樣，比較不會被 Google 誤會你要 go 去哪裡啦

## What is Elixir

依照維基百科的定義

> **Elixir** 是一個基於 [Erlang](https://zh.wikipedia.org/wiki/Erlang) [虛擬機器](https://zh.wikipedia.org/wiki/虚拟机)的[函數式](https://zh.wikipedia.org/wiki/函数式编程)、面向[並列](https://zh.wikipedia.org/wiki/并发计算)的通用[程式語言](https://zh.wikipedia.org/wiki/编程语言)。Elixir 以 Erlang 為基礎，支援分散式、高容錯、即時應用程式的開發，亦可通過[巨集](https://zh.wikipedia.org/wiki/宏)實現[元程式設計](https://zh.wikipedia.org/wiki/元编程)對其進行擴充，並通過協定支援[多型](https://zh.wikipedia.org/wiki/多态_(面向对象程序设计))。[[1\]](https://zh.wikipedia.org/zh-tw/Elixir#cite_note-1)

> José Valim 是 Elixir 語言的設計者。他創造該語言的目標是在維持與現有 Erlang 工具鏈及生態環境相容性的同時，讓人們可以在 Erlang 虛擬機器上進行擴充性更好的、高生產率的開發。[[2\]](https://zh.wikipedia.org/zh-tw/Elixir#cite_note-2)

簡單來講 Elixir

- 一個基於 Erlang 的新語言，提供更好的程式開發效率
- 是 Functional programming
- 設計者為 José Valim

優點：這邊可以說是繼承了 Erlang 的優點，Erlang 什麼地方好棒棒 Elixir 就一樣好棒棒

這裡提供 Elixir in Action 這本書提出關於 Erlang 的優點

> Fault-tolerance—A system has to keep working when something unforeseen happens.
>
> Unexpected errors occur, bugs creep in, components occasionally fail, network
>
> connections drop, or the entire machine where the system is running crashes.
>
> Whatever happens, you want to localize the impact of an error as much as possible,
>
> recover from the error, and keep the system running and providing service.

> Scalability—A system should be able to handle any possible load. Of course, you
>
> don’t buy tons of hardware just in case the entire planet’s population might start
>
> using your system some day. But you should be able to respond to a load increase
>
> by adding more hardware resources without any software intervention. Ideally,
>
> this should be possible without a system restart.

> Distribution—To make a system that never stops, you have to run it on multiple
>
> machines. This promotes the overall stability of the system: if a machine is taken
>
> down, another one can take over. Furthermore, this gives you the means to scale
>
> horizontally — you can address load increase by adding more machines to the
>
> system, thus adding work units to support the higher demand.

> Responsiveness—It goes without saying that a system should always be reasonably fast
>
> and responsive. Request handling shouldn’t be drastically prolonged, even if the
>
> load increases or unexpected errors happen. In particular, occasional lengthy tasks
>
> shouldn’t block the rest of the system or have a significant effect on performance.

> Live update—In some cases, you may want to push a new version of your software
>
> without restarting any servers. For example, in a telephone system, you don’t
>
> want to disconnect established calls while you upgrade the software.

Erlang 是由瑞典的 Ericsson 電信公司發明的，電信公司嘛講求的就是服務不中斷，由上述幾點也可以印證出了服務不中斷的精神喔

## Installing Elixir

這邊我們要開始架設環境練練手了！

如果你會用 Docker 那其實很快

```bash
docker run -it --rm elixir
```

好了後就可以直接使用了，不需要接下來的設定囉

~~

如果你在 mac 環境底下並且已經安裝了 `brew`

```bash
brew update
```

```bash
brew install elixir
```

## Setting PATH environment variable

我們來找一下我們剛剛裝在什麼地方

```bash
$ where elixir
/usr/local/bin/elixir
```

接下來就是把路徑設定到你的 shell profile 裡面

```bash
export PATH="$PATH:/usr/local/bin/elixir"
```

好了後我們重新開一個新的終端機下 

```bash
$ elixir --version
Erlang/OTP 22 [erts-10.6.2] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe] [dtrace]

Elixir 1.10.0 (compiled with Erlang/OTP 22)
```

完成！

## Hello World

每個程式最一開始當然不免俗的要來一個 Hello World 一下

最簡單的方法就是使用 interactive shell

在這模式裡面你可以下任何的 Elixir 指令並即時看其結果

```bash
$ iex

Erlang/OTP 22 [erts-10.6.2] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe] [dtrace]

Interactive Elixir (1.10.0) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> "Hello " <> "World"
"Hello World"
iex(2)> 
```

## 後記

最一開始碰 Elixir 其實是強者我同學告訴我的，但那時候滿腦子都是 OOP 就沒有多著墨，剛好最近看到六角學院看到有一個活動 w3HexSchool 鼠年全馬鐵人挑戰的活動，想到以前碰過一下下就丟角落，最近趁這個活動來嘗試一下 Functional programming 的威能啊！

## 參考資料

1. https://elixir-lang.org/getting-started/introduction.html



