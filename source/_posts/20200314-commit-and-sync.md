---
title: Visual Studio 2017 中 Commit，Commit and Push 和 Commit and Sync 差異
tags:
  - Git
  - Visual Studio 2017
date: 2020-03-14 15:04:48
---

## 前言

其實這個問題存在心中很久了，只是我一直以來只會使用 Commit and Push，所以問題就一直擺著 XD，今天我們就來介紹一下！

## 差異

在 VS2017 中，Commit 按鈕會有三個選項可以選

-   Commit
-   Commit and Push
-   Commit and Sync 

三者的差異呢？

1.  Commit 會記錄更動過的地方並且上傳（from working directory to git directory）到本地端的 repository
2.  Commit and Push 則是上面動作再加上 push 到 remote repository
3.  Commit and Sync 會做三件事情，Commit，Pull 和 Push

## 參考資料

1. https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2013/hh850436(v=vs.120)?redirectedfrom=MSDN



